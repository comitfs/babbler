<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ The MIT License (MIT)
  ~
  ~ Copyright (c) 2014-2015 Christian Schudt
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in
  ~ all copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  ~ THE SOFTWARE.
  -->

<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>rocks.xmpp</groupId>
        <artifactId>root</artifactId>
        <version>0.5.3-gltd-SNAPSHOT</version>
    </parent>
    <artifactId>xmpp-documentation</artifactId>
    <name>Babbler - Documentation</name>
    <packaging>pom</packaging>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>index</report>
                            <!--<report>project-team</report>-->
                            <!--<report>modules</report>-->
                            <!--<report>dependency-info</report>-->
                            <!--<report>dependency-management</report>-->
                            <!--<report>plugins</report>-->
                            <!--<report>cim</report>-->
                            <!--<report>issue-tracking</report>-->
                            <!--<report>scm</report>-->
                            <!--<report>dependency-convergence</report>-->
                            <!--<report>license</report>-->
                            <!--<report>plugin-management</report>-->
                            <!--<report>distribution-management</report>-->
                            <!--<report>summary</report>-->
                            <!--<report>mailing-list</report>-->
                            <!--<report>dependencies</report>-->
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>javadoc</report>
                        </reports>
                    </reportSet>
                </reportSets>
                <configuration>
                    <quiet>true</quiet>
                    <reportOutputDirectory>${project.build.directory}</reportOutputDirectory>
                    <includeDependencySources>true</includeDependencySources>
                    <includeTransitiveDependencySources>false</includeTransitiveDependencySources>
                    <nodeprecated>false</nodeprecated>
                    <nodeprecatedlist>false</nodeprecatedlist>
                    <author>false</author>
                    <notimestamp>true</notimestamp>
                    <doctitle>Babbler ${project.version}</doctitle>
                    <windowtitle>Babbler ${project.version}</windowtitle>
                    <docfilessubdirs>true</docfilessubdirs>
                    <show>public</show>
                </configuration>
            </plugin>
        </plugins>
    </reporting>

    <build>
        <plugins>
            <!-- Construct the assembly -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <id>Make assembly</id>
                        <!-- Bind the assembly generation to the package phase -->
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <finalName>babbler-${project.version}</finalName>
                    <useProjectArtifact>false</useProjectArtifact>
                    <appendAssemblyId>false</appendAssemblyId>
                    <!-- http://jira.codehaus.org/browse/MASSEMBLY-352 -->
                    <attach>true</attach>
                    <descriptorId>jar-with-dependencies</descriptorId>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>rocks.xmpp</groupId>
            <artifactId>xmpp-core</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>rocks.xmpp</groupId>
            <artifactId>xmpp-extensions</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>rocks.xmpp</groupId>
            <artifactId>xmpp-core-client</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>rocks.xmpp</groupId>
            <artifactId>xmpp-extensions-client</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>
</project>